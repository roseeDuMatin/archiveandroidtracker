package com.android.archiveandroidtracker

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @Test
    fun testLaunch(){
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.test_main_activity)).check(matches(isDisplayed()))
    }

    @Test
    fun testPipelineCantPass() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.test_fail)).check(matches(isDisplayed()))
    }

}